<?php

namespace App\Http\Middleware;

use Closure;

class HttpsProtocol
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (app()->environment('production')) 
        {
            if (substr($request->header('Host'), 0, 4)  !== 'www.')
            {
                $request->headers->set('Host', env('APP_URL'));
                return redirect()->secure($request->path(), 301);
            }

            if (!$request->secure())
            {
                return redirect()->secure($request->path(), 301);
            }
        }

        return $next($request);
    }
}