<!DOCTYPE html>

<html>
    
<head>
    
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:800,700,600,400,300">
    
</head>

<body style="font-family: 'Open Sans', sans-serif">

    <h1>Enquiry</h1>
    <p>A new enquiry has been submitted. Details are below:</p>
    
    <p>
        <strong>Name: </strong>{{ $name }}<br />
        <strong>Email Address: </strong>{{ $email }}<br />
        <strong>Enquiry Details: </strong>{{ $enquiry }}<br />
    </p>
    
</body>

</html>
