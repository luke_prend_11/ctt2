@extends('layouts.master')
@section('content')

    @include('includes.paymentNotifications')
    
    @include('layouts.courses', ['courseInfo' => $courseInfo])
    
<br class="clearfloat" />
<p>In order to obtain an SIA licence you will need to show that you are trained to the right level. This applies to front line staff only.</p>
<p>To get one of the qualifications linked to Door Supervisor licensing you will need to attend and take four training modules and pass three exams. The duration of the training should be 45 hours.</p>
<p><strong>Training leading to the licence-linked door supervision qualifications must be delivered over a minimum of FOUR days</strong>. Awarding organisations offering these qualifications will monitor training providers to ensure that their courses meet this requirement.</p>

<h2>Course Content</h2>
<p>Common Security Industry Knowledge</p>
<ul class="list-none">
    <li><strong>Session 1:</strong> Awareness of the Law in the Private Security Industry</li>
    <li><strong>Session 2:</strong> Health and Safety for the Private Security Operative</li>
    <li><strong>Session 3:</strong> Fire Safety Awareness</li>
    <li><strong>Session 4:</strong> Emergency Procedures</li>
    <li><strong>Session 5:</strong> The Private Security Industry</li>
    <li><strong>Session 6:</strong> Communication Skills and Customer Care</li>
</ul>

<p>Door Supervisor Specialist Module</p>
<ul class="list-none">
    <li><strong>Session 1:</strong> Behavioural Standards</li>
    <li><strong>Session 2:</strong> Civil and Criminal Law</li>
    <li><strong>Session 3:</strong> Searching</li>
    <li><strong>Session 4:</strong> Arrest</li>
    <li><strong>Session 5:</strong> Drugs Awareness</li>
    <li><strong>Session 6:</strong> Recording Incidents and Crime Preservation</li>
    <li><strong>Session 7:</strong> Licensing Law</li>
    <li><strong>Session 8:</strong> Emergency Procedures</li>
    <li><strong>Session 9:</strong> Dealing with Vulnerable Individuals</li>
    <li><strong>Session 10:</strong> Dealing with Queues and Crowds</li>
</ul>

<p>Conflict Management Module</p>
<ul class="list-none">
    <li><strong>Session 1:</strong> Avoiding Conflict and Reducing Personal Risk</li>
    <li><strong>Session 2:</strong> Defusing Conflict</li>
    <li><strong>Session 3:</strong> Resolving and Learning from Conflict</li>
    <li><strong>Session 4:</strong> Application of Communication Skills and Conflict Management for Door Supervisors</li>
</ul>

<p>Physical Intervention Skills Module</p>
<ul class="list-none">
    <li><strong>Session 1:</strong> Introduction to Physical Skills</li>
    <li><strong>Session 2:</strong> Disengagement Techniques</li>
    <li><strong>Session 3:</strong> Escorting Techniques</li>
</ul>

<a href="{{ route('contact') }}" title="Contact Us" class="btn dark-grey float-left">
    Contact us for more information
</a>
		
@stop