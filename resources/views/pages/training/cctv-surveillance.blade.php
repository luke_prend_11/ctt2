@extends('layouts.master')
@section('content')


    @include('includes.paymentNotifications')
    
    @include('layouts.courses', ['courseInfo' => $courseInfo])
    
    <br class="clearfloat" />
    <p>In order to obtain an SIA licence you will need to show that you are trained to the right level. This applies to front line staff only.</p>
    <p>To get one of the qualifications linked to Public Space Surveillance (CCTV) licensing you will need to attend and take two training modules and take and pass two exams. The duration of the training should be 32 hours. The course may be delivered over four days or during weekends and/or evening sessions.</p>
    
    <h2>Course Content</h2>
    <p>Common Security Industry Knowledge</p>
    <ul class="list-none">
        <li><strong>Session 1:</strong> Awareness of the Law in the Private Security Industry</li>
        <li><strong>Session 2:</strong> Health and Safety for the Private Security Operative</li>
        <li><strong>Session 3:</strong> Fire Safety Awareness</li>
        <li><strong>Session 4:</strong> Emergency Procedures</li>
        <li><strong>Session 5:</strong> The Private Security Industry</li>
        <li><strong>Session 6:</strong> Communication Skills and Customer Care</li>
    </ul>
    
    <p>Public Space Surveillance (CCTV) Operations</p>
    <ul class="list-none">
        <li><strong>Session 1:</strong> Introduction to the Roles and Responsibilities of the CCTV Operator and other CCTV Staff</li>
        <li><strong>Session 2:</strong> Codes of Practice, Operational Procedures and Guidelines</li>
        <li><strong>Session 3:</strong> CCTV Equipment and its Operation</li>
        <li><strong>Session 4:</strong> Legislation</li>
        <li><strong>Session 5:</strong> Dealing with Incidents</li>
        <li><strong>Session 6:</strong> CCTV Surveillance Techniques</li>
        <li><strong>Session 7:</strong> Emergency Procedures in the CCTV Control Room</li>
        <li><strong>Session 8:</strong> Health and Safety at Work in the CCTV Control Room</li>
    </ul>

    <a href="{{ route('contact') }}" title="Contact Us" class="btn dark-grey float-left">
        Contact us for more information
    </a>

@stop