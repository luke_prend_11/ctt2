<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseDate extends Model
{
    public $fillable = ['course_id', 'date'];
    
}
