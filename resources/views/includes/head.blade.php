    <!-- include google analytics if in production -->
    @if(App::environment('production'))
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-92671796-4"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-92671796-4');
        </script>
    @endif

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ Request::get('page_title') }}</title>
    <meta name="Description" content="{{ Request::get('meta_description') }}" />
    <meta name="Keywords" content="{{ Request::get('meta_keywords') }}" />
    <meta name="Developer" content="Luke Prendergast" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
    <link rel="stylesheet/less" type="text/css" href="{{ URL::asset('css/master.less') }}" />
    {!! Request::get('css') !!}
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Jura:300,400,700|Open+Sans:300,400,700" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('img/favicon.png') }}" />

    <!-- Force html5 elements to work in ie7 and ie8 -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->
    
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="{{ URL::asset('js/less.min.js') }}"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    
    @if(!isset($_COOKIE['eucookie']))
    <!-- If EU Cookie Law hasn't been accepted then run script -->
    <script type="text/javascript">
        function SetCookie(c_name, value, expiredays)
        {
            var exdate = new Date();
            exdate.setDate(exdate.getDate() + expiredays);
            document.cookie = c_name + "=" + escape(value) + "; path=/" + ((expiredays === null) ? "" : "; expires=" + exdate.toGMTString());
        }
    </script>
    @endif
    
    {!! Request::get('js') !!}  