
            @if ($message = Session::get('success'))
                <div class="alert alert-success" role="alert">
                    <button onclick="this.parentElement.style.display='none'" type="button" class="close" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {!! $message !!}
                </div>
                <?php Session::forget('success');?>
            @endif

            @if ($message = Session::get('error'))
                <div class="alert alert-danger" role="alert">
                    <button onclick="this.parentElement.style.display='none'" type="button" class="close" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {!! $message !!}
                </div>
                <?php Session::forget('error');?>
            @endif
