<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    @include('includes.head')
    
</head>
<body>
    @if(!isset($_COOKIE['eucookie']))
        <!-- If EU Cookie Law hasn't been accepted then display message and run script -->
        <div id="eucookielaw" >
            <div class="content">
                <p>This website uses cookies to track visitor activity and behaviour. By browsing our site you agree to our use of cookies.</p>
                <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                    <div class="btn-group mr-2" role="group" aria-label="First group">
                        <button id="removecookie" title="Accept Cookies" class="btn blue">Accept</button>
                    </div>
                    <div class="btn-group mr-2" role="group" aria-label="Second group">
                        <a id="more" href="{{ route('privacy') }}" title="Privacy Policy" class="btn dark-grey">Find Out More</a>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            if(document.cookie.indexOf("eucookie") === -1 ) {
                $("#eucookielaw").show();
            }
            $("#removecookie").click(function () {
                SetCookie('eucookie','eucookie',365*10);
                $("#eucookielaw").remove();
            });
        </script>
    @endif
    
    <header>
        @include('includes.header')
            
    </header>

    <div class="light-grey">
        <div class="container-fluid">
            <!-- show breadcrumb if not home page -->
            @if (Route::currentRouteName() != 'home')
             <div class="row">
                <div class="col-xs-12 col-md-12">
                    {!! Breadcrumbs::render() !!}
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <h1>{{ Request::get('h1') }}</h1>

                    @yield('content')
                    
                </div>
            </div>
        </div>
    </div>

    <footer>
        @include('includes.footer')
            
    </footer>
    
    <!-- Smooth scroll -->
    <script type="text/javascript" src="{{ URL::asset('js/smooth-scroll.js') }}"></script>

    <!-- show/hide function -->
    <script>
    $(document).ready(function(){
        $("h3.toggle").click(function(){
            $("div.toggle").toggle();
        });
    });
    </script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
</body>
</html>