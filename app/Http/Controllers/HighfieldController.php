<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HighfieldController extends Controller
{
    private function login()
    {
        $data = array(
            'username' => env('HIGHFIELD_API_USERNAME'), 
            'password' => env('HIGHFIELD_API_PASSWORD')
        );

        return $this->getCurlData($data, "/login");
    }
    
    private function getCurlData($data, $url)
    {
        $ch = curl_init(); // START CURL
        curl_setopt($ch,CURLOPT_URL, env('HIGHFIELD_API_URL').$url); // CURL DESTINATION
        curl_setopt($ch, CURLOPT_POST, 1); // SETTING CURL AS TYPE 'POST'
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // APPLYING DATA AS POST VARIABLES
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // MUST BE SET TO TRUE TO STORE RETURN
        $response = curl_exec($ch);  // STORE RESPONSE AGAINST VARIABLE
        curl_close($ch); // CLOSE/TERMINATE CURL   

        return $response;
    }
    
    public function functions()
    {
        $data = array();
        $url  = "/functions";
        
        return $this->runFunction($data, $url, true);
    }
    
    public function courses()
    {
        $data = array('token' => $login_response->data->token);
        $url  = "/getcourses";
        
        return $this->runFunction($data, $url, true);
    }
    
    public function get_companies()
    {
        $data = array('token' => $login_response->data->token);
        $url  = "/gettiers";
        
        return $this->runFunction($data, $url);
    }
    
    public function get_children($id)
    {
        $data = array('token' => $login_response->data->token,'id' => $id);  
        $url  = "/getTierChildren";
        
        return $this->runFunction($data, $url);
    }
    
    public function get_learners()
    {
        $data = array('token' => $login_response->data->token);    
        $url  = "/getlearners";
        
        return $this->runFunction($data, $url);
    }

    public function validateUser()
    {
        // RUN DESIRED FUNCTION
        $data = array();
        $data['token'] = $login_response->data->token;
        // ADD MANAGER AT THE SAME TIME NOT REQUIRED
        $data['firstname'] = "[INSERT USER FIRST NAME]"; 
        $data['lastname'] = "[INSERT USER LAST NAME]";
        $data['password'] = "[INSERT USER PASSWORD]";
        $data['email'] = "[ENTER A VALID EMAIL ADDRESS]"; // Enter a valid email address for the user   
        
        $url = "/validateuser";
        
        return $this->runFunction($data, $url);
    }
    
    public function add_company()
    {
        // RUN DESIRED FUNCTION
        $data = array();
        $data['token'] = $login_response->data->token;
        // $data['parent_id'] = [INSERT PARENT ID]; // INT - ONLY NEEDED IF ADDING A DIV OR SITE
        $data['name'] = "[INSERT TIER NAME]";
        // ADD MANAGER AT THE SAME TIME NOT REQUIRED   
        $data['user_firstname'] = "[INSERT USER FIRST NAME]";
        $data['user_lastname'] = "[INSERT USER LAST NAME]";
        $data['user_password'] = "[INSERT USER PASSWORD]";
        $data['user_email'] = "[INSERT USER EMAIL ADDRESS]";
        
        $url  = "/addtier";
        
        return $this->runFunction($data, $url);
    }
    
    public function setLearnerPassword($learner_un, $learner_pw, $login_aid = null)
    {
        $data = array(
            'token' => $login_response->data->token, 
            'username' => $learner_un, 
            'password' => $learner_pw, 
        );

        if($login_aid)
        {
            $data['account_id'] = $login_aid;
        } 
        
        $url = "/setLearnerPassword";
        
        return $this->runFunction($data, $url);
    }

    // ENROL LEARNER
    // YOU CAN EDIT THIS FUNCTION TO ACCEPT A LEARNER VIA VARIABLES - RATHER THAN HARDCODED
    public function autoenrol()
    {
        $data = array();
        $data['token'] = $login_response->data->token;       

        // ADD LEARNER
        $data['firstname'] = "[INSERT LEARNERS FIRST NAME]";
        $data['lastname'] = "[INSERT LEARNERS LAST NAME]";
        $data['username'] = "[INSERT LEARNERS USERNAME]";
        $data['email'] = "[INSERT LEARNERS EMAIL ADDRESS]";
        $data['password'] = "[INSERT LEARNERS PASSWORD]";
        $data['course_id'] = "[INSERT COURSE ID]"; // INT
        $data['enrol_to_id'] = "[INSERT TIER ID]"; // INT   
        
        $url = "/autoenrol";
        
        return $this->runFunction($data, $url);
    }
    
    public function createlicenses()
    {
        // RUN DESIRED FUNCTION
        $data = array();
        $data['token'] = $login_response->data->token;            

        // ADD LEARNER
        $data['destination_id'] = "[INSERT TIER ID]"; // INT
        $data['course_id'] = "[INSERT COURSE ID]"; // INT
        $data['amount'] = "[INSERT AMOUNT TO CREATE]"; // INT    
        
        $url = "/createLicences";
        
        return $this->runFunction($data, $url);
    }

    private function format_json($json, $html = false)
    {
        $tabcount = 0;
        $result = '';
        $inquote = false;
        $ignorenext = false; 

        if($html)
        { 
            $tab = "&nbsp;&nbsp;&nbsp;";
            $newline = "<br/>";
        }
        else
        {
            $tab = "\t";
            $newline = "\n";
        } 

        for($i = 0; $i < strlen($json); $i++)
        {
            $char = $json[$i];
            
            if($ignorenext)
            { 
                $result .= $char;
                $ignorenext = false;
            }
            else
            {
                switch($char)
                {
                    case '{':
                        $tabcount++;
                        $result .= $char . $newline . str_repeat($tab, $tabcount);
                        break;
                    case '}':
                        $tabcount--;
                        $result = trim($result) . $newline . str_repeat($tab, $tabcount) . $char;
                        break;
                    case ',':
                        $result .= $char . $newline . str_repeat($tab, $tabcount);
                        break;
                    case '"':
                        $inquote = !$inquote;
                        $result .= $char;
                        break;
                    case '\\': 
                        if($inquote) $ignorenext = true;
                        $result .= $char;
                        break;
                    default:
                        $result .= $char;
                }
            }
        }
        return $result;
    } 
    
    private function runFunction($data, $url, $html)
    {
        $login_to_api = $this->login();

        // MAKE SURE YOU GET A RESPONSE FROM THE API
        if($login_to_api)
        {
            // DECODE RESPONSE
            $login_response = json_decode($login_to_api);
            
            if($login_response->success && $login_response->data->token)
            {
                $response = $this->getCurlData($data, $url);

                if($response)
                {
                    // DECODE RESPONSE
                    $response = $this->format_json($response, $html);
                    return $response;
                }
                else
                {
                    // UNABLE TO MAKE A CONNECTION TO THE API
                    // CREATE YOUR OWN FAIL MESSAGE
                }            
            }
            else
            {
                // INVALID LOGIN
                return $login_response;
            }
        }
        else
        {
            // UNABLE TO MAKE A CONNECTION TO THE API
            // CREATE YOUR OWN FAIL MESSAGE
        }
    }
}
