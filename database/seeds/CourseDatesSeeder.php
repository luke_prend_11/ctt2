<?php

use Illuminate\Database\Seeder;

class CourseDatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('course_dates')->insert([
            [
                "course_id" => 1,
                "date" => date("2019-02-11 09:00:00"),
            ],
            [
                "course_id" => 2,
                "date" => date("2019-02-18 09:00:00"),
            ],
            [
                "course_id" => 3,
                "date" => date("2019-07-01 09:00:00"),
            ],
            [
                "course_id" => 4,
                "date" => date("2019-03-04 09:00:00"),
            ],
            [
                "course_id" => 5,
                "date" => date("2019-03-11 09:00:00"),
            ],
            [
                "course_id" => 1,
                "date" => date("2019-03-04 09:00:00"),
            ],
            [
                "course_id" => 1,
                "date" => date("2019-03-11 09:00:00"),
            ],
        ]);
    }
}
