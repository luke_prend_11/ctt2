<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use App\Payment as PaymentModel;

class PaymentController extends Controller
{
    private $_api_context;
    
    public function __construct(Request $request)
    {
        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret']
        ));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function payWithpaypal(Request $request)
    {
        \Session::put('returnRoute', url()->previous());
        
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        
        $item_1 = new Item();
        $item_1->setName($request->get('course')) /** item name **/
            ->setSku($request->get('id'))
            ->setCurrency('GBP')
            ->setQuantity(1)
            ->setPrice($request->get('amount')); /** unit price **/
        
        $item_list = new ItemList();
        $item_list->setItems(array($item_1));
        
        $amount = new Amount();
        $amount->setCurrency('GBP')
            ->setTotal($request->get('amount'));
                
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Course Booking: '.$request->get('course'));
                
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('status')) /** Specify return URL **/
            ->setCancelUrl(URL::route('status'));
                
        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
                
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                \Session::put('error', 'Connection timeout');
                return Redirect::to(session()->get('returnRoute'));
            } else {
                \Session::put('error', 'Some error has occurred, sorry for any inconvenience.');
                return Redirect::to(session()->get('returnRoute'));
            }
        }
        
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        
        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
            /** redirect to paypal **/
            return Redirect::away($redirect_url);
        }
        
        \Session::put('error', 'Unknown error occurred');
        return Redirect::to(session()->get('returnRoute'));
    }
    
    public function getPaymentStatus()
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
        /** clear the session payment ID **/
        
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token')))
        {
            \Session::put('error', 'Payment failed');
            return Redirect::to(session()->get('returnRoute'));
        }
        
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
        
        if ($result->getState() == 'approved')
        {
            $this->insertPaymentReference($result);
            
            \Session::put('success', 'Thank you. Your payment was successful.');
            return Redirect::to(session()->get('returnRoute'));
        }
        
        \Session::put('error', 'Payment failed');
        return Redirect::to(session()->get('returnRoute'));
    }
    
    private function insertPaymentReference($result)
    {
        PaymentModel::create([
            'merchant_id' => $result->transactions[0]->payee->merchant_id,
            'name' => $result->transactions[0]->item_list->shipping_address->recipient_name,
            'post_code' => $result->transactions[0]->item_list->shipping_address->postal_code,
            'email' => $result->transactions[0]->payee->email,
            'course_name' => $result->transactions[0]->item_list->items[0]->name,
            'course_id' => $result->transactions[0]->item_list->items[0]->sku,
            'price' => $result->transactions[0]->item_list->items[0]->price,
        ]);
    }
}