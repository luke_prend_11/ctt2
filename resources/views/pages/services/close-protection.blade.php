@extends('layouts.master')
@section('content')

<p>{{ Config::get('constants.SITE_NAME') }} management team have well over 60 years combined experience in the Close Protection Industry to clients from entertainment, sport and music industries, Diplomats, Middle Eastern Royalty and Members of Parliament.</p>
<p>{{ Config::get('constants.SITE_NAME') }} have supplied Close Protection to clients throughout the UK, USA, Europe, Saudi Arabia and Africa.</p>
<p>In the current world it is not uncommon for our Close Protection Officers to be utilised by clients from corporate or business backgrounds who understand the need for discreet and professional personal protection.</p>
<a href="{{ route('contact') }}" title="Contact Us" class="btn dark-grey float-left">
    Contact us for more information
</a>
		
@stop