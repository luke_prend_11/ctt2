<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public $fillable = ['merchant_id', 'name', 'post_code', 'email', 'course_name', 'course_id', 'price'];
}
