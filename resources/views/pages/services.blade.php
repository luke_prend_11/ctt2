@extends('layouts.master')
@section('content')

<p>At CTT, we offer the following services:</p>
<ul class="list-none">
    <li>
        <a href="{{ route('services-close-protection') }}" title="Close Protection Services">Close Protection</a>
    </li>
    <li>
        <a href="{{ route('services-retail-security') }}" title="Retail Security Services">Retail Security</a>
    </li>
    <li>
        <a href="<{{ route('services-manned-guarding') }}" title="Manned Guarding Services">Manned Guarding</a>
    </li>
    <li>
        <a href="{{ route('services-security-guards') }}" title="Security Guard Services">Provision of Security Guards</a>
    </li>
</ul>

<a href="{{ route('contact') }}" title="Contact Us" class="btn dark-grey float-left">
    Contact us for more information
</a>
		
@stop