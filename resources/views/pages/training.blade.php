@extends('layouts.master')
@section('content')

<p>At CTT, we offer the following courses:</p>
<ul class="list-none">
    <li>
        <a href="{{ route('training-close-protection') }}" title="Close Protection Training">Level 3 Certificate in Close Protection</a>
    </li>
    <li>
        <a href="{{ route('training-door-supervision') }}" title="Door Supervision Training">Level 2 Award in Door Supervision, plus Up Skills for Door Supervisors</a>
    </li>
    <li>
        <a href="{{ route('training-cctv-surveillance') }}" title="CCTV Surveillance Training">Level 2 in CCTV (Public Space Surveillance) Operations</a>
    </li>
    <li>
        <a href="{{ route('training-conflict-management') }}" title="Conflict Management Training">Conflict Management Level 2</a>
    </li>
    <li>
        <a href="{{ route('training-physical-intervention') }}" title="Physical Intervention Training">Physical Intervention Level 2 and Level 3</a>
    </li>
    <li>
        <a href="{{ route('training-first-aid') }}" title="First Aid Training">Emergency First Aid Level 2 and First Aid at Work</a>
    </li>
</ul>

<a href="{{ route('contact') }}" title="Contact Us" class="btn dark-grey float-left">
    Contact us for more information
</a>
		
@stop