@extends('layouts.master')
@section('content')

<span class="btn blue float-left">1 Day Course: £75</span>
<br class="clearfloat" />
<h2>Physical Intervention Skills</h2>
<p>The Physical Intervention Skills unit for the Private Security Industry (as per the new full qualification) will be assessed by multi choice examination and practical assessment.</p>

<h2>Safety Awareness for Door Supervisors</h2>
<p>The new Safety Awareness for Door Supervisors unit has been developed to cover the following subjects within the security sector:</p>

<ul class="list-points">
    <li>Dealing with young people</li>
    <li>First Aid Awareness</li>
    <li>Counter Terrorism</li>
</ul>
<p>There will be a multi choice examination for the module; Safety Awareness for Door Supervisors.</p>

<a href="{{ route('contact') }}" title="Contact Us" class="btn dark-grey float-left">
    Contact us for more information
</a>
		
@stop