<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request;
use App\Course;
use Carbon\Carbon;

class CoursesController extends Controller
{
    private $courseId;
    
    public function index()
    {
        $slug           = Request::segment(count(Request::segments()));
        $this->courseId = $this->setCourseId($slug);
        $courseInfo     = $this->getCourseInfo();
        
        return view('pages.training.'.$slug)
                ->with('courseInfo', $courseInfo);
    }
    
    private function setCourseId($slug)
    {
        switch ($slug) {
            case "door-supervision":
                $id = 1;
                break;
            case "cctv-surveillance":
                $id = 2;
                break;
            case "close-protection":
                $id = 3;
                break;
            case "first-aid":
                $id = array(4, 5);
                break;
        }
        
        return $id;
    }
    
    private function getCourseInfo()
    {
        $query = is_array($this->courseId) ? Course::whereIn('id', $this->courseId) : Course::where('id', $this->courseId);
                
        return $query->with(['courseDates' => function($query) {
            $query->where('date', '>=', Carbon::now());
        }])->get();
    }
}
