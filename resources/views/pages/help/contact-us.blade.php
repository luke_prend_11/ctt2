@extends('layouts.master')
@section('content')

            </div>
        </div>
    </div>
</div>

<div class="light-grey">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-md-8">
                <form class="form-horizontal" method="POST" action="{{ route('contact-form-submit') }}">
                    {{ csrf_field() }}

                    @if(isset($success))
                    <div class="alert alert-success">
                        <strong>{{ $success }}</strong>
                    </div>
                    @endif

                    <p>If you have any questions about our services or training courses or you are requesting to hire our Security Services, please use the form below to contact us.</p>

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <input id="name" type="text" class="form-control" placeholder="Name" name="name" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="email" class="form-control" placeholder="E-Mail Address" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('comments') ? ' has-error' : '' }}">
                        <textarea id="comments" class="form-control" placeholder="Comments" name="comments" rows="5" required>{{ old('comments') }}</textarea>

                        @if ($errors->has('comments'))
                            <span class="help-block">
                                <strong>{{ $errors->first('comments') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                    </div>
                </form>
            </div>

            <div class="col-xs-12 col-md-4">
                <div id="contact-info" class="very-light-grey">
                    <h2>Contact Information</h2>

                    <ul class="list-none">
                        <li><i class="far fa-building"></i>{{ Config::get('constants.CONTACT_ADDRESS') }}</li>
                        <li>
                            <a href="tel:{{ Config::get('constants.CONTACT_PHONE') }}" title="Call {{ Config::get('constants.SITE_NAME') }}">
                                <i class="fas fa-phone"></i>
                                {{ Config::get('constants.CONTACT_PHONE') }}
                            </a>
                        </li>
                        <li>
                            <a href="mailto:{{ Config::get('constants.CONTACT_EMAIL') }}" title="Email {{ Config::get('constants.SITE_NAME') }}">
                                <i class="fas fa-envelope"></i>
                                {{ Config::get('constants.CONTACT_EMAIL') }}
                            </a>
                        </li>
                    </ul>
                </div>

@stop
