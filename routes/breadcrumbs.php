<?php

// Home
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Home', route('home'));
});


Breadcrumbs::register('services', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Services', route('services'));
});

Breadcrumbs::register('services-close-protection', function ($breadcrumbs) {
    $breadcrumbs->parent('services');
    $breadcrumbs->push('Close Protection', route('services-close-protection'));
});

Breadcrumbs::register('services-retail-security', function ($breadcrumbs) {
    $breadcrumbs->parent('services');
    $breadcrumbs->push('Retail Security', route('services-retail-security'));
});

Breadcrumbs::register('services-manned-guarding', function ($breadcrumbs) {
    $breadcrumbs->parent('services');
    $breadcrumbs->push('Manned Guarding', route('services-manned-guarding'));
});

Breadcrumbs::register('services-security-guards', function ($breadcrumbs) {
    $breadcrumbs->parent('services');
    $breadcrumbs->push('Security Guards', route('services-security-guards'));
});


Breadcrumbs::register('training', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Training', route('training'));
});

Breadcrumbs::register('training-close-protection', function ($breadcrumbs) {
    $breadcrumbs->parent('training');
    $breadcrumbs->push('Close Protection', route('training-close-protection'));
});

Breadcrumbs::register('training-door-supervision', function ($breadcrumbs) {
    $breadcrumbs->parent('training');
    $breadcrumbs->push('Door Supervision', route('training-door-supervision'));
});

Breadcrumbs::register('training-cctv-surveillance', function ($breadcrumbs) {
    $breadcrumbs->parent('training');
    $breadcrumbs->push('CCTV Surveillance', route('training-cctv-surveillance'));
});

Breadcrumbs::register('training-conflict-management', function ($breadcrumbs) {
    $breadcrumbs->parent('training');
    $breadcrumbs->push('Conflict Management', route('training-conflict-management'));
});

Breadcrumbs::register('training-physical-intervention', function ($breadcrumbs) {
    $breadcrumbs->parent('training');
    $breadcrumbs->push('Physical Intervention', route('training-physical-intervention'));
});

Breadcrumbs::register('training-first-aid', function ($breadcrumbs) {
    $breadcrumbs->parent('training');
    $breadcrumbs->push('First Aid', route('training-first-aid'));
});


Breadcrumbs::register('help', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Help', route('help'));
});

Breadcrumbs::register('contact', function ($breadcrumbs) {
    $breadcrumbs->parent('help');
    $breadcrumbs->push('Contact Us', route('contact'));
});

Breadcrumbs::register('contact-form-submit', function ($breadcrumbs) {
    $breadcrumbs->parent('help');
    $breadcrumbs->push('Contact Us', route('contact-form-submit'));
});

Breadcrumbs::register('about', function ($breadcrumbs) {
    $breadcrumbs->parent('help');
    $breadcrumbs->push('About Us', route('about'));
});

Breadcrumbs::register('privacy', function ($breadcrumbs) {
    $breadcrumbs->parent('help');
    $breadcrumbs->push('Privacy', route('privacy'));
});


Breadcrumbs::register('', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('404', '');
});