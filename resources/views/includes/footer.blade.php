<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-md-2">
            <ul class="list-none">
                <li><strong><a href="{{ route('home') }}" title="Home Page">Home</a></strong></li>
                <li><a href="{{ route('about') }}" title="About Us">About Us</a></li>
                <li><a href="{{ route('privacy') }}" title="Privacy Policy">Privacy Policy</a></li>
            </ul>
        </div>

        <div class="col-xs-12 col-md-3">
            <ul class="list-none">
                <li><strong><a href="{{ route('services') }}" title="Our Services">Services</a></strong></li>
                <li><a href="{{ route('services-close-protection') }}" title="Close Protection">Close Protection</a></li>
                <li><a href="{{ route('services-retail-security') }}" title="Retail Security">Retail Security</a></li>
                <li><a href="{{ route('services-manned-guarding') }}" title="Manned Guarding">Manned Guarding</a></li>
                <li><a href="{{ route('services-security-guards') }}" title="Provision of Security Guards">Provision of Security Guards</a></li>
            </ul>
        </div>

        <div class="col-xs-12 col-md-3">
            <ul class="list-none">
                <li><strong><a href="{{ route('training') }}" title="Training">Training</a></strong></li>
                <li><a href="{{ route('training-close-protection') }}" title="Close Protection">Close Protection</a></li>
                <li><a href="{{ route('training-door-supervision') }}" title="Door Supervision">Door Supervision</a></li>
                <li><a href="{{ route('training-cctv-surveillance') }}" title="CCTV Surveillance">CCTV Surveillance</a></li>
                <li><a href="{{ route('training-conflict-management') }}" title="Conflict Management">Conflict Management</a></li>
                <li><a href="{{ route('training-physical-intervention') }}" title="Physical Intervention">Physical Intervention</a></li>
                <li><a href="{{ route('training-first-aid') }}" title="First Aid">First Aid</a></li>
            </ul>
        </div>

        <div class="col-xs-12 col-md-4">
            <ul class="list-none">
                <li><strong><a href="{{ route('contact') }}" title="Contact Us">Contact Us</a></strong></li>
                <li>
                    <a href="tel:{{ Config::get('constants.CONTACT_PHONE') }}" title="Call {{ Config::get('constants.SITE_NAME') }}">
                        <i class="fas fa-phone"></i>
                        {{ Config::get('constants.CONTACT_PHONE') }}
                    </a>
                </li>
                <li>
                    <a href="mailto:{{ Config::get('constants.CONTACT_EMAIL') }}" title="Email {{ Config::get('constants.SITE_NAME') }}">
                        <i class="fas fa-envelope"></i>
                        {{ Config::get('constants.CONTACT_EMAIL') }}
                    </a>
                </li>
                <li>
                    <a href="https://facebook.com/{{ Config::get('constants.FACEBOOK_ID') }}" title="{{ Config::get('constants.SITE_NAME') }} on Facebook" target="_blank">
                        <i class="fab fa-facebook-square"></i>
                        Like on Facebook
                    </a>
                </li>
                <li>
                    <a href="{{ Config::get('constants.LINKED_IN') }}" title="{{ Config::get('constants.SITE_NAME') }} on LinkedIn" target="_blank">
                        <i class="fab fa-linkedin"></i>
                        Connect on LinkedIn
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>

<div id="copyright">
    <span>
        {{ Config::get('constants.SITE_NAME') }}  © {{ Carbon\Carbon::today()->format('Y') }}. All Rights Reserved | 
        Website Developed by <a href="https://lpwebdesign.co.uk" title="LP Web Design" target="_blank">LP Web Design</a>
    </span>
</div>
