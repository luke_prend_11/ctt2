<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ route('home') }}"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbar">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('home') }}"><span>CTT</span> Home</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span>Our</span> Services
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item" href="{{ route('services-close-protection') }}" title="Close Protection">Close Protection</a>
                        <a class="dropdown-item" href="{{ route('services-retail-security') }}" title="Retail Security">Retail Security</a>
                        <a class="dropdown-item" href="{{ route('services-manned-guarding') }}" title="Manned Guarding">Manned Guarding</a>
                        <a class="dropdown-item" href="{{ route('services-security-guards') }}" title="Provision of Security Guards">Provision of Security Guards</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown02" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Training <span>Courses</span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdown02">
                        <a class="dropdown-item" href="{{ route('training-close-protection') }}" title="Close Protection Training">Close Protection</a>
                        <a class="dropdown-item" href="{{ route('training-door-supervision') }}" title="Door Supervision Training">Door Supervision</a>
                        <a class="dropdown-item" href="{{ route('training-cctv-surveillance') }}" title="CCTV Surveillance Training">CCTV Surveillance</a>
                        <a class="dropdown-item" href="{{ route('training-conflict-management') }}" title="Conflict Management Training">Conflict Management</a>
                        <a class="dropdown-item" href="{{ route('training-physical-intervention') }}" title="Physical Intervention Training">Physical Intervention</a>
                        <a class="dropdown-item" href="{{ route('training-first-aid') }}" title="First Aid Training">First Aid</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('contact') }}">Contact <span>Us</span></a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div id="header-image"></div>

<div id="contact-bar">
    <div class="container-fluid">
        <ul class="list-none">
            <li>
                <a href="tel:{{ Config::get('constants.CONTACT_PHONE') }}" title="Call {{ Config::get('constants.SITE_NAME') }}">
                    <i class="fas fa-phone"></i>
                    {{ Config::get('constants.CONTACT_PHONE') }}
                </a>
            </li>
            <li>
                <a href="mailto:{{ Config::get('constants.CONTACT_EMAIL') }}" title="Email {{ Config::get('constants.SITE_NAME') }}">
                    <i class="fas fa-envelope"></i>
                    {{ Config::get('constants.CONTACT_EMAIL') }}
                </a>
            </li>
        </ul>
    </div>
</div>