
    <div class="courses float-left">
        <h3>{{ $courseInfo->days }} Day Course: £{{ $courseInfo->price }}</h3>
        <ul class="list-none">
            @if(count($courseInfo->courseDates) > 0)
                @foreach ($courseInfo->courseDates as $courseDate)
                <li>
                    {{ date('l j F Y', strtotime($courseDate->date)) }} 
                    <a href="{{ route('paypal', [
                        'amount' => $courseInfo->price, 
                        'course' => $courseInfo->title.' - '.date('d m y', strtotime($courseDate->date)), 
                        'id' => $courseInfo->id]) }}" class="btn">
                        Book a Place
                    </a>
                </li>
                @endforeach
            @else
            <li>No courses scheduled. <br /><a href="{{ route('contact') }}" title="Contact us">Contact us</a> to find out when the next course will take place.</li>
            @endif
        </ul>
    </div>
