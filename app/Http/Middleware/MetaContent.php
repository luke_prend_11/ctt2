<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Request;

class MetaContent
{
    const DEFAULT_META_KEYWORDS    = "close Protection, counter terrorism, manned guarding, close protection training";
    const DEFAULT_META_DESCRIPTION = "Corporate Task Training Ltd offer the very best in Security, Health &amp; Safety training, not only for the individual but for your entire work force and company.";
    const STYLESHEET_LINK          = "<link rel=\"stylesheet/less\" type=\"text/css\" href=\"%s/css/%s\" />";
    const JAVASCRIPT_LINK          = "<script type=\"text/javascript\" src=\"%s\"></script>";
    const HEADER_IMAGE_LINK        = "images/header-images/%s.jpg";
    
    private $allMetaContent;
    private $currentPageMeta;
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->setAllMetaContent();
        $this->setCurrentPageRoute();
        
        $request->attributes->add([
            'page_title' => $this->setPageTitle(),
            'meta_description' => $this->setMetaDescription(),
            'meta_keywords' => $this->setMetaKeywords(),
            'h1' => $this->setPageH1(),
            'css' => $this->setCss(),
            'js' => $this->setJs()
        ]);
        
        return $next($request);
    }
    
    private function setAllMetaContent()
    {
        $this->allMetaContent = array(
            "_" => array(
                "page_title"       => "Close Protection Training &amp; Services",
                "meta_description" => config('constants.SITE_NAME')." offer the very best in Security, Health & Safety training, not only for the individual but for your entire work force and company.",
                "h1"               => config('constants.SITE_NAME'),
            ),
            "error" => array(
                "page_title"       => "Page Not Found",
                "meta_description" => "The page you have requested has not been found.",
                "h1"               => "Page Not Found",
            ),
            "site_map" => array(
                "page_title"       => "Site Map",
                "meta_description" => "Links to all pages on ".config('constants.SITE_NAME'),
                "h1"               => "Site Map",
            ),
            "services" => array(
                "page_title"       => "Protection Services",
                "meta_description" => config('constants.SITE_NAME')." offer the the following services: Close Protection, Retail Security, Manned Guarding, Provision of Security Guards",
                "h1"               => "Our Services",
            ),
            "services_close_protection" => array(
                "page_title"       => "Close Protection Services",
                "meta_description" => config('constants.SITE_NAME')." have supplied Close Protection to clients throughout the UK, USA, Europe, Saudi Arabia and Africa.",
                "h1"               => "Close Protection",
            ),
            "services_manned_guarding" => array(
                "page_title"       => "Manned Guarding",
                "meta_description" => config('constants.SITE_NAME')." can provide a twenty first century version of the &ldquo;Night Watchman&rdquo; to patrol and protect your sites.",
                "h1"               => "Manned Guarding",
            ),
            "services_retail_security" => array(
                "page_title"       => "Retail Security",
                "meta_description" => config('constants.SITE_NAME')." have been providing retail security solutions to businesses nationwide for many years.",
                "h1"               => "Retail Security",
            ),
            "services_security_guards" => array(
                "page_title"       => "Provison of Security Guards",
                "meta_description" => "All our security guards and staff are fully trained, licensed in accordance with SIA (Security Industry Authority) Legislation and screened to BS7858 (5 year work or back to school checkable history & CRB Cleared).",
                "h1"               => "Provison of Security Guards",
            ),
            "training" => array(
                "page_title"       => "Training Courses",
                "meta_description" => config('constants.SITE_NAME')." offer the very best in Security, Health & Safety training, not only for the individual but for your entire work force and company.",
                "h1"               => "Training Courses",
            ),
            "training_cctv_surveillance" => array(
                "page_title"       => "Public Space Surveillance (CCTV) Training",
                "meta_description" => "Public Space Surveillance (CCTV) Training by ".config('constants.SITE_NAME')." Ltd",
                "h1"               => "Public Space Surveillance (CCTV) Training",
            ),
            "training_close_protection" => array(
                "page_title"       => "Close Protection Training Level 3",
                "meta_description" => "The Close Protection course is run by our highly qualified trainers who have over 100 years of CP experience combined.",
                "h1"               => "Close Protection Training Level 3",
            ),
            "training_conflict_management" => array(
                "page_title"       => "Conflict Management Training",
                "meta_description" => config('constants.SITE_NAME')."'s Conflict Management Training course description.",
                "h1"               => "Conflict Management Training",
            ),
            "training_door_supervision" => array(
                "page_title"       => "Door Supervisor Training Courses",
                "meta_description" => "The Door Supervisor Training consists of 30 hours contact training time over 4 days which our trainers will be there to mentor you on.",
                "h1"               => "Door Supervisor Training",
            ),
            "training_first_aid" => array(
                "page_title"       => "First Aid at Work",
                "meta_description" => config('constants.SITE_NAME')." First Aid training courses.",
                "h1"               => "First Aid at Work",
            ),
            "training_physical_intervention" => array(
                "page_title"       => "Physical Intervention Training Level 2 and Level 3",
                "meta_description" => config('constants.SITE_NAME')."'s Physical Intervention Training for Level 2 and Level 3.",
                "h1"               => "Physical Intervention Training Level 2 and Level 3",
            ),
            "help" => array(
                "page_title"       => "Help",
                "meta_description" => "With years of experience and fully qualified staff we are able to offer a professional and reliable close protection service.",
                "h1"               => "Help",
            ),
            "about_us" => array(
                "page_title"       => "About Us",
                "meta_description" => "With years of experience our fully qualified staff are able to offer a professional close protection services and training.",
                "h1"               => "About Us",
            ),
            "help_contact_us" => array(
                "page_title"       => "Contact Us",
                "meta_description" => "Contact ".config('constants.SITE_NAME').".",
                "h1"               => "Contact Us",
                "css"              => "contact.less"
            ),
            "help_privacy" => array(
                "page_title"       => "Privacy Policy",
                "meta_description" => "Find out more about our privacy policy.",
                "h1"               => "Privacy Policy",
            ),
            "404" => array(
                "page_title"       => "Page Not Found",
                "meta_description" => "The page you are trying to visit does not exist. Please contact us for more information.",
                "h1"               => "Page Not Found",
            )
        );
    }
    
    private function setCurrentPageRoute()
    {
        $getPageRoute          = $this->removeSpecialCharacters(Request::path());
        $currentPageRoute      = array_has($this->allMetaContent, $getPageRoute) ? $getPageRoute : "404";
        $this->currentPageMeta = $this->allMetaContent[$currentPageRoute];
    }
    
    private function removeSpecialCharacters($string)
    {
        $toReplace   = array("/","-");
        $replaceWith = array("_","_");
        
        return str_replace($toReplace, $replaceWith, $string);
    }
    
    private function setPageTitle()
    {
        return isset($this->currentPageMeta["page_title"]) && !empty($this->currentPageMeta["page_title"])
               ? $this->currentPageMeta["page_title"].config('constants.PAGE_TITLE_SEPARATOR').config('constants.SITE_NAME')
               : config('constants.SITE_NAME');
        
    }
    
    private function setMetaDescription()
    {
        return isset($this->currentPageMeta["meta_description"]) && !empty($this->currentPageMeta["meta_description"])
               ? $this->currentPageMeta["meta_description"] 
               : self::DEFAULT_META_DESCRIPTION;
    }

    private function setMetaKeywords()
    {
        return isset($this->currentPageMeta["meta_keywords"]) && !empty($this->currentPageMeta["meta_keywords"])
               ? $this->currentPageMeta["meta_keywords"] 
               : self::DEFAULT_META_KEYWORDS;
    }

    private function setPageH1()
    {
        return isset($this->currentPageMeta["h1"]) && !empty($this->currentPageMeta["h1"])
               ? $this->currentPageMeta["h1"] 
               : $this->setPageTitle();
    }

    private function setCss()
    {
        if(isset($this->currentPageMeta["css"]))
        {
            return $this->buildAsset($this->currentPageMeta["css"]);
        }
    }

    private function setJs()
    {
        if(isset($this->currentPageMeta["js"]))
        {
            return $this->buildAsset($this->currentPageMeta["js"]);
        }
    }

    private function buildAsset($asset)
    {
        if(is_array($asset))
        {
            $string = "";
            foreach ($asset as $assetLink)
            {
                $string .= $this->createAssetLink($assetLink);
                $string .= "\n";
            }
        }
        else
        {
            $string = $this->createAssetLink($asset);
            $string .= "\n";
        }
        return $string;
    }

    private function createAssetLink($asset)
    {
        return strpos($asset, 'css') || strpos($asset, 'less') !== false 
               ? sprintf(self::STYLESHEET_LINK, URL::to('/'), $asset) 
               : sprintf(self::JAVASCRIPT_LINK, $asset);
    }
}
