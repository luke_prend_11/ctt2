<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public $fillable = ['title', 'price'];
    
    /**
     * Get the course date record associated with the course.
     */
    public function courseDates()
    {
        return $this->hasMany('App\CourseDate');
    }
}
