@extends('layouts.master')
@section('content')

            <p>The page you are looking for is unavailable. If you entered a web address please check it is correct.</p>
            <p>We have recently updated our website so you may have been directed to a page that is no longer available. If you would like more information on something then please contact us and we will be happy to help.</p>
            
            <div class="text-center">
                <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                    <div class="btn-group mr-2" role="group" aria-label="First group">
                        <a href="{{ route('home') }}" class="btn blue" title="Go to Homepage">Visit Homepage</a>
                    </div>
                    <div class="btn-group mr-2" role="group" aria-label="Second group">
                        <a href="{{ route('contact') }}" class="btn blue" title="Contact Us">Contact Us</a>
                    </div>
                </div>
            </div>
            
@stop