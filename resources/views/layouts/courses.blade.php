@if (is_object($courseInfo))
    @foreach ($courseInfo as $course)
        @include('layouts.courseList', ['courseInfo' => $course])
        
    @endforeach
@else
    @include('layouts.courseList', ['courseInfo' => $courseInfo])
@endif
