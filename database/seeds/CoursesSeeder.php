<?php

use Illuminate\Database\Seeder;

class CoursesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')->insert([
            [
                "title" => "Door Supervisor",
                "price" => 199,
                "days" => 4,
            ],
            [
                "title" => "CCTV Surveillance",
                "price" => 185,
                "days" => 4,
            ],
            [
                "title" => "Close Protection",
                "price" => 2999,
                "days" => 28,
            ],
            [
                "title" => "Emergency First Aid",
                "price" => 99,
                "days" => 1,
            ],
            [
                "title" => "First Aid",
                "price" => 265,
                "days" => 3,
            ],
        ]);
    }
}
