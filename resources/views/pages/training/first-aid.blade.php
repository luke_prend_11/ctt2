@extends('layouts.master')
@section('content')

@include('includes.paymentNotifications')

@include('layouts.courses', ['courseInfo' => $courseInfo])
    
<br class="clearfloat">

<p>The Health and Safety Executive (HSE) Approved 3-Day First Aid at Work Training Course provides training for qualification as a First Aider within the workplace under the guidance of the Health &amp; Safety Regulations 1981.</p>

<a href="{{ route('contact') }}" title="Contact Us" class="btn dark-grey float-left">
    Contact us for more information
</a>

@stop