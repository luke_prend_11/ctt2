<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.home');
})->name('home');


Route::get('/services', function () {
    return view('pages.services');
})->name('services');

Route::get('/services/close-protection', function () {
    return view('pages.services.close-protection');
})->name('services-close-protection');

Route::get('/services/retail-security', function () {
    return view('pages.services.retail-security');
})->name('services-retail-security');

Route::get('/services/manned-guarding', function () {
    return view('pages.services.manned-guarding');
})->name('services-manned-guarding');

Route::get('/services/security-guards', function () {
    return view('pages.services.security-guards');
})->name('services-security-guards');


Route::get('/training', function () {
    return view('pages.training');
})->name('training');

Route::get('/training/close-protection', 'CoursesController@index')->name('training-close-protection');
Route::get('/training/door-supervision', 'CoursesController@index')->name('training-door-supervision');
Route::get('/training/cctv-surveillance', 'CoursesController@index')->name('training-cctv-surveillance');
Route::get('/training/first-aid', 'CoursesController@index')->name('training-first-aid');

Route::get('/training/conflict-management', function () {
    return view('pages.training.conflict-management');
})->name('training-conflict-management');

Route::get('/training/physical-intervention', function () {
    return view('pages.training.physical-intervention');
})->name('training-physical-intervention');


Route::get('/help', function () {
    return view('pages.help');
})->name('help');

Route::get('/help/contact-us', function () {
    return view('pages.help.contact-us');
})->name('contact');

Route::post('/help/contact-us', 'ContactController@submit')->name('contact-form-submit');

Route::get('/about-us', function () {
    return view('pages.help.about-us');
})->name('about');

Route::get('/help/privacy', function () {
    return view('pages.help.privacy');
})->name('privacy');


Route::get('/functions', 'HighfieldController@functions');
Route::get('/courses', 'HighfieldController@courses');


Route::get('paypal', 'PaymentController@paywithpaypal')->name('paypal');
Route::get('status', 'PaymentController@getPaymentStatus')->name('status');
