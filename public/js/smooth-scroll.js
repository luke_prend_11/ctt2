// smooth scroll to external anchor links
var jsvar = document.getElementsByClassName("breadcrumb");

if (jsvar) {
    $(document).ready(function(){
        $('html, body').animate({
                scrollTop: $(jsvar[0]).offset().top + 50
        }, 2000);
        return false;
    });
}
