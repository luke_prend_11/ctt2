@extends('layouts.master')
@section('content')

<p>If you would like any further information regarding <strong>{{ config('constants.SITE_NAME') }}</strong> or if you would like to apply for a place on one of our training courses then contact us using the details on our contact page.</p>

<a href="{{ route('contact') }}" title="Contact Us" class="btn dark-grey float-left">
    Contact us
</a>

@stop
