<?php

return [
    'SITE_NAME' => 'Corporate Task Training',
    'SHORT_SITE_NAME' => 'CTT',
    'PAGE_TITLE_SEPARATOR' => ' | ',
    'CONTACT_PHONE' => '0121 507 0638',
    'CONTACT_EMAIL' => 'enquiries@corporatetasktraining.com',
    'CONTACT_EMAIL_DOMAIN' => '@corporatetasktraining.com',
    'CONTACT_ADDRESS' => '102 Spencer Street, Birmingham, B18 6DB',
    'FACEBOOK_ID' => 'closetacticaltraining',
    'LINKED_IN' => 'http://www.linkedin.com/pub/james-ford/25/b4/23b',
];
