<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
           $table->increments('id');
           $table->string('merchant_id')->default('');
           $table->string('name')->default('');
           $table->string('post_code')->default('');
           $table->string('email')->default('');
           $table->string('course_name')->default('');
           $table->tinyInteger('course_id')->default(0);
           $table->float('price')->default(0);
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payments');
    }
}
